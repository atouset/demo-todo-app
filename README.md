# Demo Todo App

Full stack todo manager using Angular and Spring Boot


## Built With

* [Spring Boot](https://spring.io/projects/spring-boot/) - Spring Data JPA, Spring Security, Spring MVC, H2 Database
* [Maven](https://maven.apache.org/) - Dependency Management
* [Angular 8](https://angular.io/) - Used for entire front-end

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for demonstration purposes.

### Prerequisites

- JDK 8.*
- NPM version 6 or higher
- Angular CLI version 7 or higher
- Maven 3.*

### Installing

1) Download/clone the project to local workspace

2) Launch back-end: Navigate into 'restful-web-services' directory and build with Maven. 
```
mvn clean install
```
3) Run the application
```
java -jar target/restful-web-services-0.0.1-SNAPSHOT.jar
```
-Ensure project is running on localhost:8080

4) Navigate back up into 'todo' directory (from project root folder) and run the UI. 
```
ng serve
```
-Application should be live at localhost:4200

### Usage

To login, use the following credentials:

| username  | password |
| --------- | -------- |
| superdev  | dummy    |


The system uses Spring Security and JWT for authentication.

After logging in, View a list of todos, add, and delete as you wish!

Logout to clear session and hide navigation.

## Contributing

This is for resume demo purposes only, not an active project.

## Authors

* **Anthony Touset** - *Initial work* 
