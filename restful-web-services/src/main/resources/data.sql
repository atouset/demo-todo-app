insert into todo(id, username, description, target_date, is_done) values (10001,'SuperDev','Learn JPA', sysdate(), false);

insert into todo(id, username, description, target_date, is_done) values (10002,'SuperDev','Learn DATA JPA', sysdate(), false);

insert into todo(id, username, description, target_date, is_done) values (10003,'SuperDev','Learn Microservices', sysdate(), false);

insert into tag(id, name, todo_id) values (1001,'extracuricular', 10001);
insert into tag(id, name,todo_id) values (1002,'sports', 10001);
insert into tag(id, name, todo_id) values (1003,'tech', 10002);
insert into tag(id, name, todo_id) values (1004,'racing', 10003);
insert into tag(id, name, todo_id) values (1005,'music', 10003);