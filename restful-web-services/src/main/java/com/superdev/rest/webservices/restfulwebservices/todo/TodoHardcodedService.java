package com.superdev.rest.webservices.restfulwebservices.todo;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Service;

@Service
public class TodoHardcodedService {

	private static List<Todo> todos = new ArrayList<>();
	private static List<Tag> tags = new ArrayList<>();
	
	private static Integer idCounter = 0;
	private static Integer tagCounter = 0;
	
	static {
		tags.add(new Tag(++tagCounter, "Extracuricular"));
		tags.add(new Tag(++tagCounter, "Tech"));
		
		todos.add(new Todo(++idCounter, "superdev", "Learn to Dance", new Date(), false));
		todos.add(new Todo(++idCounter, "superdev", "Learn about Microservices", new Date(), false));
		todos.add(new Todo(++idCounter, "superdev", "Learn about Angular", new Date(), false));
	}
	
	public List<Todo> findAll(){
		
		return todos;
	}
	
	public Todo deleteById(Integer id) {
		Todo todo = findById(id);
		if (todo == null) {
			return null;
		}
		if(todos.remove(todo)) {
			return todo;
		};
		return null;
		
	}

	public Todo findById(Integer id) {
		for (Todo todo: todos) {
			if (todo.getId() == id) {
				return todo;
			}
		}
		return null;
	}
	
	public Todo save(Todo todo) {
		if (todo.getId()==-1 || todo.getId()==0) {
			todo.setId(++idCounter);
			todos.add(todo);
		} else {
			deleteById(todo.getId());
			todos.add(todo);
		}
		return todo;
	}
}
