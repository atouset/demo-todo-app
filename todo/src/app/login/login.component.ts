import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { HardcodedAuthenticationService } from '../service/hardcoded-authentication.service';
import { BasicAuthenticationService } from '../service/basic-authentication.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  username = 'superdev'
  password = ''
  errorMessage = 'Invalid Credentials'
  invalidLogin = false

  //Router
  //Dependency Injection


  constructor(
    private router: Router, 
    private hardCodedAuthenticationService: HardcodedAuthenticationService,
    private basicAuthenticationService: BasicAuthenticationService,
    ) { }

  ngOnInit() {
  }

  handleLogin(){    
    //if (this.username ==="SuperDev" && this.password === 'dummy'){
    if(this.hardCodedAuthenticationService.authenticate(this.username, this.password)){  
    this.invalidLogin = false 
      console.log("Invalid Login: " + this.invalidLogin)
      //redirect to welcome page
      this.router.navigate(['welcome', this.username])
    } else {
      this.invalidLogin = true
    }
    // console.log(this.username);
    // console.log(this.password);
  }

  handleBasicAuthLogin(){    
    //if (this.username ==="SuperDev" && this.password === 'dummy'){
    this.basicAuthenticationService.executeAuthenticationService(this.username, this.password)
      .subscribe(
        data => {
          console.log(data)
          //redirect to welcome page
          this.router.navigate(['welcome', this.username])
          this.invalidLogin = false
        },
        error => {
          console.log(error)
          this.invalidLogin = true
        }
      )     
    // console.log(this.username);
    // console.log(this.password);
  }
  
  handleJWTAuthLogin(){    
    //if (this.username ==="SuperDev" && this.password === 'dummy'){
    this.basicAuthenticationService.executeJWTAuthenticationService(this.username, this.password)
      .subscribe(
        data => {
          console.log(data)
          //redirect to welcome page
          this.router.navigate(['welcome', this.username])
          this.invalidLogin = false
        },
        error => {
          console.log(error)
          this.invalidLogin = true
        }
      )     
    // console.log(this.username);
    // console.log(this.password);
  }


}
